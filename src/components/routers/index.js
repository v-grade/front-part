import { createRouter, createWebHistory } from 'vue-router'
import GeneratePromoCode from '../promo/GeneratePromoCodes.vue'
import PromocodesListForAdmin from '../promo/PromocodesListForAdmin.vue'
import Promocodes from '../promo/Promocodes.vue'
import MainPage from '../MainPage.vue'
import Halls from '../halls/Halls.vue'
import HallsListForAdmin from '../halls/HallsListForAdmin.vue'
import Hall from '../halls/Hall.vue'
import Services from '../services/Services.vue'
import ServiceListForAdmin from '../services/ServiceListForAdmin.vue'
import Service from '../services/Service.vue'
import AddNewService from '../services/AddNewService.vue'
import AddNewTrainer from '../trainers/AddNewTrainer.vue'
import AddNewHall from '../halls/AddNewHall.vue'
import Trainers from '../trainers/Trainers.vue'
import TrainerListForAdmin from '../trainers/TrainerListForAdmin.vue'
import Trainer from '../trainers/Trainer.vue'
import CreateSubscription from '../subscription/CreateSubscription.vue'
import Subscriptions from '../subscription/Subscriptions.vue'
import Subscription from '../subscription/Subscription.vue'

const router = createRouter({
    routes: 
    [
        {
            path: "/admin",
            redirect: "/admin/trainers"
        },
        {
            path: "/",
            redirect: "/main"
        },
        {
            path: "/main",
            component: MainPage
        },
        {
            path: "/halls",
            component: Halls
        },
        {
            path: "/services",
            component: Services
        },
        {
            path: "/trainers",
            component: Trainers
        },
        {
            path: "/admin/trainers",
            component: TrainerListForAdmin
        },
        {
            path: "/admin/trainers/new",
            component: AddNewTrainer
        },
        {
            path: "/admin/trainers/:id",
            component: Trainer
        },
        {
            path: "/admin/halls/new",
            component: AddNewHall
        },
        {
            path: "/admin/halls",
            component: HallsListForAdmin
        },
        {
            path: "/admin/halls/:id",
            component: Hall
        },
        {
            path: "/admin/promocodes/new",
            component: GeneratePromoCode
        },
        {
            path: "/admin/services",
            component: ServiceListForAdmin
        },
        {
            path: "/admin/services/new",
            component: AddNewService
        },
        {
            path: "/admin/services/:id",
            component: Service
        },
        {
            path: "/admin/promocodes",
            component: PromocodesListForAdmin
        },
        {
            path: "/promocodes",
            component: Promocodes
        },
        {
            path: "/admin/subscriptions",
            component: Subscriptions
        },
        {
            path: "/admin/subscriptions/:id",
            component: Subscription
        },
        {
            path: "/create-sub",
            component: CreateSubscription
        }
    ],
    history: createWebHistory()
})

export default router